<?php
/*
Plugin Name: Produtos Certificado Digital
Description: Adicionar função de receber produtos e imprimir na tela como shortcode
Version: 1.0.0
Author:Guilherme M @ tecnobiz
*/

/*******************************************************
NOVO BOX DE PRODUTO
 *******************************************************/
function get_produto_certificado( $atts ) {
	// Attributes
	extract( shortcode_atts(
			array(
				'idarray' => '0',
				'prodmac' => '0',
			), $atts )
	);
	$html = "";

	if($idarray != "0") {
		$ids = explode(",", $idarray);

		$indicados_mac = explode(",", $prodmac);

		$i=0;
		foreach ($ids as $key => $id) {
			$imgThumbsOBJ = simplexml_load_string(get_the_post_thumbnail( $id, 'full'));
			$imgThumbsURL = $imgThumbsOBJ->attributes()->src;
			$linkcompra = get_post_meta( $id, 'linkcompra', true );

			if (get_option("modo_manutencao") == "1") {
				$url = get_option("modo_manutencao_link")."/?url=";
			} elseif($linkcompra == '1'){
				$url = $GLOBALS["LINK_LOJA"]."CarrinhoCompras.aspx?CompraCupom=";
			} elseif ($linkcompra == '0') {
				$url = $GLOBALS["LINK_CARRINHO"];
			} else {
				$url = $GLOBALS["LINK_CARRINHO"];
			}
			if($i == 0) {
				$html .=	'<div class="row z-index-position"><div class="col-xs-12">';
				$html .=		'<div class="prod-group grupo-de-produtos">';
				$html .=		'<div class="image col-md-2 col-xs-12">';
				$html .=			'<img src="'.$imgThumbsURL.'">';
				$html .=		'</div>';
				$html .=		'<div class="produtos-list col-md-6 col-sm-8 col-xs-12"><div class="row box-produto-certificado">';
				$html .= '<span class="dispositivo">Dispositivos</span>';
			}

			$idProduct = 'product-' .$i;
			$classProduct = 'inputs-group-' .$i;

			$html .= 	'<div class="col-lg-6 produto produto-box">';
			$html .= 		'<span name="img" style="display:none;">'.$imgThumbsURL.'</span>';
			$html .=        "<input id='$idProduct' class='produto-input $classProduct' type='radio' name='product-chose'>";
			$html .= 		'<h2 class="produto-titulo prod-title">'.get_the_title($id).'</h2>';


			$html .= 		'<div class="prod-months" style="display: none">';
			$html .= 		'<div class="center-months" style="display: none;">';
			$iT = 0;
			for ($iV=1; $iV <= 3; $iV++) {
				$tempo = get_post_meta($id, "tempo".$iV, true);
				if($tempo != '') {
					$link = get_post_meta($id, "link".$iV, true);
					$cupom = get_post_meta($id, "cupom".$iV, true);
					$valor = get_post_meta($id, "valor".$iV, true);
					$valor = number_format(((double)get_post_meta($id, "valor".$iV, true)), 2, ',', '.');
					$valor_de = get_post_meta($id, "valor_de_".$iV, true);
					$parcelas = get_post_meta($id, "parcelas".$iV, true);

					if(!empty($valor_de)) {
						$valor_de = number_format(((double)$valor_de), 2, ',', '.');
					} else {
						$valor_de = "";
					}

					$valor_parcelado = 0;
					if($parcelas){
						$original_value = $valor;
						$valor = (float) str_replace(',', '.', $valor);
						$numero = (float) $valor/$parcelas;
						$valor_parcelado = number_format($numero, 2, ',', '.');
						$valor = $original_value;
					}
					$valor_mes = number_format(((float)$valor/$tempo), 2, ',', '.');
					$urlP = ($linkcompra == '1') ? $url.$cupom : $url.$link;
					$last = ($iT == 0) ? "last" : "";
					$active = ($iT == 0 && $i == 0) ? "active" : "";
					$html .= 	'<div class="prod-month '.$last.' '.$active.'">';
					$html .= 		"<input type='radio' name='meses' class='month-inner $classProduct' value='$tempo' $active>";
					$html .= 			'<strong>'.$tempo.'</strong>meses';
					$html .= 		'<span name="valorDe" style="display:none;">'.$valor_de.'</span>';
					$html .= 		'<span name="valor" style="display:none;">'.$valor.'</span>';
					$html .= 		'<span name="valorParc" style="display:none;">'.$valor_parcelado.'</span>';
					$html .= 		'<span name="quantParc" style="display:none;">'.$parcelas.'</span>';
					$html .= 		'<span name="link" style="display:none;">'.$urlP.'</span>';
					$html .= 	'</div>';
					$html .= 	apply_filters("serasa/get_produto_certificado", '', $id, $tempo);

					$iT++;
				}
			}
			$html .= 		'</div>';
			$html .= 		'</div>';

			if (in_array($id, $indicados_mac)) {
				$html .= '<div class="prod-mac hidden-xs">Modelo indicado para uso no Mac</div>';
			}

			$html .=			'<div class="produto-info visible-xs">';
			$html .=				'<div class="info-body">';
			$html .= 					'<span class="real-price"></span>';
			$html .=					'<span class="price"><span class="xsmall">R$</span>'.$valor.'</span>';
			$html .=					'<div class="prod-info">apenas '.$parcelas.'x de R$ '.$valor_parcelado.'</div>';
			$html .=					'<a class="btn" href="'.$urlP.'">Comprar</a>';
			$html .=					'<br>';
			$html .=					'<span class="desc">Equivale a apenas R$ '.$valor_mes.' por mês</span>';

			if (in_array($id, $indicados_mac)) {
				$html .= '<br style="clear: both;"><div class="prod-mac">Modelo indicado para uso no Mac</div>';
			}

			$html .=				'</div>';
			$html .=			'</div>';
			$html .= 	'</div>';

			if($i == 0) {
				$end = "";
				$end .=		'</div>';

				$end .= 	'</div>';
				$end .=		'<div class="produto-info col-md-4 col-sm-4 hidden-xs">';
				$end .=			'<div class="info-header">'.get_the_title($id).'<span>'.$tempo. ' meses de validade</span></div>';
				$end .=			'<div class="info-body">';
				$end .=				'<span class="real-price"></span>';
				$end .=				'<span class="price"><span class="xsmall">R$</span>'.$valor.'</span>';
				$end .=				'<div class="prod-info">apenas '.$parcelas.'x de R$ '.$valor_parcelado.'</div>';
				$end .=				'<a class="btn" href="'.$urlP.'">Comprar</a>';
				$end .=			'</div>';
				$end .=		'</div>';
				$end .=		'</div>';
				$end .=	'</div></div>';
			}
			$i++;
		}
		$html .= $end;

	}
	return $html;
}
add_shortcode( 'getProdutosCertificado', 'get_produto_certificado' );